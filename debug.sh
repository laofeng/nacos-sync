#该命令用于对nacosSync进行调试，该命令需要复制到应用包中去执行，当前的路径为：nacossync-distribution/target/nacosSync.0.4.6/nacosSync
cd nacossync-distribution/target/nacosSync.0.4.6/nacosSync
java -server -Xms512m -Xmx512m -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000 -XX:-OmitStackTraceInFastThrow -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=nacossync_java_heapdump.hprof -XX:-UseLargePages -Dspring.config.location=conf/application.properties -DnacosSync.home=nacosSync -Xloggc:logs/nacossync_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -jar nacosSync-server.jar --logging.config=conf/logback-spring.xml


