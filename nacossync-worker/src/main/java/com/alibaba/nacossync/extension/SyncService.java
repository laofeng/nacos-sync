/*
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license agreements. See the NOTICE
 * file distributed with this work for additional information regarding copyright ownership. The ASF licenses this file
 * to You under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.alibaba.nacossync.extension;

import com.alibaba.nacossync.constant.SkyWalkerConstants;
import com.alibaba.nacossync.pojo.model.TaskDO;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * @author NacosSync
 * @author fenglibin
 * @version $Id: SyncManagerService.java, v 0.1 2018-09-25 下午5:17 NacosSync Exp $$
 */
public interface SyncService {
	
	/**
     * 停止同步任务
     *
     * @param taskDO
     * @return
     */
    boolean stop(TaskDO taskDO);

    /**
     * delete the sync task
     *
     * @param taskDO
     * @return
     */
    boolean delete(TaskDO taskDO);

    /**
     * execute sync
     *
     * @param taskDO
     * @return
     */
    boolean sync(TaskDO taskDO);

    /**
     * 原方法实现只是判断了原实例MetaData中是否包含了原数据源，用于判断原实例是否是同步自其它系统，
     * 如果是原实例是同步于其它系统，就不能够再往另外的系统进行同步。
     * 但是考虑Nacos可以在不同的实例中同步，如Eureka同步到Nacos，Nacos还可以同步到Eureka、ZK、Consul等，
     * 因而只需要判断实例MetaData中来自的源，与要同步的目标不同相同的系统即可。
     * 例如：
     * 某实例由Eureka同步到Nacos，但是该实例就不能够再由Nacos同步回Eureka，否则会彼此循环同步，但是该实例可以同步到其它目标，如ZK等。
     * 
     * @param sourceMetaData
     * @param taskDO
     * @return
     */
    default boolean needSync(Map<String, String> sourceMetaData, TaskDO taskDO) {
    	String sourceClusterId = sourceMetaData.get(SkyWalkerConstants.SOURCE_CLUSTERID_KEY);
        return StringUtils.isBlank(sourceClusterId) || !sourceClusterId.contentEquals(taskDO.getDestClusterId());
    }

    /**
     * Determines whether the source cluster ID of the current instance is the same as the source
     * cluster ID of the task
     */
    default boolean needDelete(Map<String, String> destMetaData, TaskDO taskDO) {
        return StringUtils.equals(destMetaData.get(SkyWalkerConstants.SOURCE_CLUSTERID_KEY),
                taskDO.getSourceClusterId());
    }

}
