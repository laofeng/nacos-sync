package com.alibaba.nacossync;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.nacossync.NacosSyncMain;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = NacosSyncMain.class)
public abstract class BaseServiceTest {

}
