package com.alibaba.nacossync.extension.eureka;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.cloud.netflix.eureka.http.RestTemplateTransportClientFactory;

import com.netflix.discovery.shared.Application;
import com.netflix.discovery.shared.Applications;
import com.netflix.discovery.shared.resolver.DefaultEndpoint;
import com.netflix.discovery.shared.resolver.EurekaEndpoint;
import com.netflix.discovery.shared.transport.EurekaHttpClient;
import com.netflix.discovery.shared.transport.EurekaHttpResponse;

public class EurekaHttpClientTest {
	private String eurekaUrl = "http://eureka-test.laidiantech.com/eureka";
	private EurekaHttpClient eurekaHttpClient;
	@Before
	public void before() {
		RestTemplateTransportClientFactory restTemplateTransportClientFactory =
                new RestTemplateTransportClientFactory();
		EurekaEndpoint eurekaEndpoint = new DefaultEndpoint(eurekaUrl);
		eurekaHttpClient = restTemplateTransportClientFactory.newClient(eurekaEndpoint);
	}
	/**
	 * 获取单个应用
	 */
	@Test
	public void test1() {
		EurekaHttpResponse<Application> response = eurekaHttpClient.getApplication("ERP-USER");
		System.out.println(response.getLocation());
	}
	
	/**
	 * 获取所有的应用
	 */
	@Test
	public void test2() {
		EurekaHttpResponse<Applications> response = eurekaHttpClient.getApplications(new String[]{});
		List<Application> list = response.getEntity().getRegisteredApplications();
		list.forEach(a->{
			System.out.println(a.getName());
		});
	}
}
