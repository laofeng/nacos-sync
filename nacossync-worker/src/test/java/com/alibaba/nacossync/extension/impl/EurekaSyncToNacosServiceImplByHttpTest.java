package com.alibaba.nacossync.extension.impl;

import java.io.UnsupportedEncodingException;

import org.junit.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacossync.util.HttpUtil;

/**
 * @author paderlol
 * @date: 2019-01-12 20:58
 * @see EurekaSyncToNacosServiceImpl
 */
public class EurekaSyncToNacosServiceImplByHttpTest {
	@Test
	public void testSyncUsingHttp() throws NacosException, InterruptedException, NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException, UnsupportedEncodingException {

		String nacosServerUrl = "http://127.0.0.1:8848/nacos/v1/ns/instance";

		// 组装提交的值
		MultiValueMap<String, String> uriVariables = new LinkedMultiValueMap<>();
		uriVariables.add("ip", "192.168.38.218");
		uriVariables.add("port", "10000");
		uriVariables.add("namespaceId", "DEV");
		uriVariables.add("weight", "1");
		uriVariables.add("enabled", "true");
		uriVariables.add("healthy", "true");
		uriVariables.add("metadata", "{\"preserved.register.source\": \"SPRING_CLOUD\"}");
		uriVariables.add("clusterName", "service-test");
		uriVariables.add("serviceName", "service-test");
		uriVariables.add("groupName", "DEFAULT_GROUP");
		uriVariables.add("ephemeral", "false");

		// 发起HTTP注册请求
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(nacosServerUrl).queryParams(uriVariables);
		System.out.println("发起注册的URL:"+builder.toUriString());
		String response = HttpUtil.sendPost(builder.toUriString());
		System.out.println(response);

		// 组装提交的值
		uriVariables = new LinkedMultiValueMap<>();
		uriVariables.add("ip", "192.168.38.218");
		uriVariables.add("port", "20000");
		uriVariables.add("namespaceId", "DEV");
		uriVariables.add("weight", "1");
		uriVariables.add("enabled", "true");
		uriVariables.add("healthy", "true");
		uriVariables.add("metadata", "{\"preserved.register.source\": \"SPRING_CLOUD\"}");
		uriVariables.add("clusterName", "service-test");
		uriVariables.add("serviceName", "service-test");
		uriVariables.add("groupName", "DEFAULT_GROUP");
		uriVariables.add("ephemeral", "false");

		builder = UriComponentsBuilder.fromHttpUrl(nacosServerUrl).queryParams(uriVariables);
		System.out.println("发起注册的URL:"+builder.toUriString());
		response = HttpUtil.sendPost(builder.toUriString());
		System.out.println(response);
		// 发起HTTP注册请求
	}

}
